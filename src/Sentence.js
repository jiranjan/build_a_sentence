import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  display: flex;
  align-self: center;
`;

const SentenceText = styled.h4`
  color: rgb(149, 211, 93);
`;

const Sentence = ({ wordsObj }) => {
  const sentence : String = Object.values(wordsObj).join(' ');
  return (
    <Wrapper>
      <SentenceText>
        {sentence}
      </SentenceText>
    </Wrapper>
  );
};

export default Sentence;
