import React from 'react';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';

import Sentence from './Sentence';
import { addToSentence } from './store/actions/sentenceActions';
const questions = require('./questions');

const Wrapper = styled.div`
  display: flex;
  flex: 1;
  flex-direction: row;
`;

const Container = styled.div`
  width: 40%;
  height: 90%;
  margin: auto;
`;

const HeadingContainer = styled.div`
  display: flex;
  flex-direction: row;
`;

const Heading = styled.h1`
  color: #FFF;
`;

const Tags = styled.h1`
  color: rgb(120, 179, 145);
`;

const QuestionContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 60%;
`;

const QuestionName = styled.h4`
  color: #FFF;
`;

const QuestionInput = styled.input`
  width: 100%;
  background: transparent;
  border: none;
  border-bottom: 2px solid #FFF;
  outline: none;
  color: #FFF;
  font-size: 14px;
  font-weight: 600;
`;

const App = () => {
  const dispatch = useDispatch();
  const wordsObj : Object = useSelector(state => state.completeSentence);
  return (
    <Wrapper>
      <Container>
        <HeadingContainer>
          <Tags>##</Tags>
          <Heading>Build A Sentence</Heading>
        </HeadingContainer>
        {
          questions.map((question, index) => (
            <QuestionContainer key={index}>
              <QuestionName>{question}</QuestionName>
              <QuestionInput
                type="text"
                onChange={(e) => dispatch(addToSentence(question, e.target.value))} />
            </QuestionContainer>
          ))
        }
      </Container>
      <Container>
        <Sentence wordsObj={wordsObj} />
      </Container>
    </Wrapper>
  );
}

export default App;
