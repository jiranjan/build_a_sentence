import { ADD_TO_SENTENCE } from './types';

export const addToSentence = (question, payload) => ({
  type: ADD_TO_SENTENCE,
  question,
  payload
});
