import {
  ADD_TO_SENTENCE,
} from '../actions/types';

const initialState : Object = {
  "Who": "",
  "What": "",
  "Where": "",
  "When": ""
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_TO_SENTENCE:
      return Object.assign({}, state, {
        [action.question.slice(0, -1)]: action.payload
      });
    default:
      return state;
  };
};
